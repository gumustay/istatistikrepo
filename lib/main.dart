import 'package:flutter/material.dart';
import 'ui/ana_sayfa.dart';
import 'ui/sliver.dart';
import 'ui/sliver_grid.dart';
import 'ui/grafik.dart';
import 'ui/grafik3.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Kocaeli Milli Eğitim Müdürlüğü İstatistik',
      theme: ThemeData(
        primarySwatch: mapBoxBlue,
      ),
      home: AnaSayfa(),
      routes: <String, WidgetBuilder>{
        slivergrid.route: (context) => slivergrid(),
        sliverliste.route: (context) => sliverliste(),
        graph1.route: (context) => graph1(),
        graph3.route: (context) => graph3(),

      },
    );
  }
}

// Generated using Material Design Palette/Theme Generator
// http://mcg.mbitson.com/
// https://github.com/mbitson/mcg
const int _bluePrimary = 0xFF395afa;
const MaterialColor mapBoxBlue = MaterialColor(
  _bluePrimary,
  <int, Color>{
    50: Color(0xFFE7EBFE),
    100: Color(0xFFC4CEFE),
    200: Color(0xFF9CADFD),
    300: Color(0xFF748CFC),
    400: Color(0xFF5773FB),
    500: Color(_bluePrimary),
    600: Color(0xFF3352F9),
    700: Color(0xFF2C48F9),
    800: Color(0xFF243FF8),
    900: Color(0xFF172EF6),
  },
);
