import 'package:flutter/material.dart';
import 'drawer.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'ilceler.dart';

class slivergrid extends StatefulWidget {
  static const String route = 'slivergrid';

  @override
  AnaSayfaState createState() {
    return new AnaSayfaState();
  }
}

class AnaSayfaState extends State<slivergrid> {

  List<String> items1;
  List<Ozet> futureOzet = [];

  // ilk çalıştırılacak yer
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getAllRehber().then((data) {
      setState(() {
        futureOzet = data;

      });
    });



  }


  Future<List<Ozet>> getAllRehber() async {
    var res;
    List liste = [];
    final response = await http.get(Uri.parse('http://10.0.2.2/phpmyadmin/istatistik/get_ozet.php'));
    //print('getEmployees Response: ${response.body}');
    if (response.statusCode == 200) {

      List<Ozet> list = parseResponse(response.body);
      //print('deneme');
      return list;

    }else {
      return List<Ozet>();
    }
  }

  static List<Ozet> parseResponse(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Ozet>((json) => Ozet.fromJson(json)).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
       drawer: buildDrawer(context, slivergrid.route),
        body: CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
            pinned: true,
            expandedHeight: 120.0,
            flexibleSpace: FlexibleSpaceBar(
              title: Text('Özet Bilgiler'),
            ),

        ),

        SliverGrid(

          gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 200.0,
            mainAxisSpacing: 10.0,
            crossAxisSpacing: 10.0,
            childAspectRatio: 4.0,

          ),
          delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
              return Container(
                alignment: Alignment.center,
                  //color: Colors.teal[100 * (index % 9)],
                  color: Colors.teal,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,


                    children: <Widget>[
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          //color: Colors.red,
                          child: Text('${futureOzet[index].baslik}',style: TextStyle(fontSize: 10.0, color: Colors.black)),
                        ),
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          //color: Colors.red,
                          child: Text('${futureOzet[index].sayi}',style: TextStyle(fontSize: 15.0, color: Colors.white)),
                        ),
                      ),


                      //Text('Başlık',style: TextStyle(fontSize: 32.0, color: Colors.black45)),
                      //Text('${itm[index]}',style: TextStyle(fontSize: 32.0, color: Colors.white)),


                    ],

                  )
              );
            },
            childCount: futureOzet.length,
          ),
        ),

      ],
    ),
    );
  }

}