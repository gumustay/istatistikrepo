import 'package:flutter/material.dart';
import 'drawer.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'ilceler.dart';

class AnaSayfa extends StatefulWidget {
  static const String route = '/';

  @override
  AnaSayfaState createState() {
    return new AnaSayfaState();
  }
}

class AnaSayfaState extends State<AnaSayfa> {
  List<List<Ozet>> tumListe;
  List<String> items1;
  List<String> items2;
  List countries = [];
  List<Ozet> futureOzet = [];
  List<Ozet> futureOzet1 = [];
  List<Ozet> futureOzet2 = [];

  // ilk çalıştırılacak yer
  @override
  void initState() {
    // TODO: implement initState
    super.initState();



    getAllRehber().then((data) {
      setState(() {
        futureOzet = data;

      });
    });

    getAllRehber2().then((data2) {
      setState(() {
        futureOzet1 = data2;
      });
    });

    getAllRehber3().then((data3) {
      setState(() {
        futureOzet2 = data3;

      });
    });



  }


  Future<List<Ozet>> getAllRehber() async {
    var res;
    List liste = [];
    final response = await http.get(Uri.parse('http://10.0.2.2/phpmyadmin/istatistik/get_ozet1.php'));
    //print('getEmployees Response: ${response.body}');
    if (response.statusCode == 200) {

      List<Ozet> list = parseResponse(response.body);

      return list;

    }else {
      return List<Ozet>();
    }
  }

  Future<List<Ozet>> getAllRehber2() async {
    var res;
    List liste = [];
    final response = await http.get(Uri.parse('http://10.0.2.2/phpmyadmin/istatistik/get_ozet2.php'));

    if (response.statusCode == 200) {

      List<Ozet> list = parseResponse(response.body);

      return list;

    }else {
      return List<Ozet>();
    }
  }

  Future<List<Ozet>> getAllRehber3() async {
    var res;
    List liste = [];
    final response = await http.get(Uri.parse('http://10.0.2.2/phpmyadmin/istatistik/get_ozet3.php'));

    if (response.statusCode == 200) {

      List<Ozet> list = parseResponse(response.body);

      return list;

    }else {
      return List<Ozet>();
    }
  }

  static List<Ozet> parseResponse(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Ozet>((json) => Ozet.fromJson(json)).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Özet Bilgiler')),
      drawer: buildDrawer(context, AnaSayfa.route),
      body: FutureBuilder<List>(


          builder: (context, snapshot) {
            return   _buildContent();


          }
      ),
    );
  }


  Widget _buildContent() {
    return ListView.builder(
        itemCount: 3,
        itemBuilder: (BuildContext content, int index) {

          return _buildHorizontalList(parentIndex: index);
        });
  }

  Widget _buildHorizontalList({int parentIndex}) {
    var colors = [
      Colors.green,
      Colors.blue,
      Colors.indigo,
      Colors.red,
      Colors.orange
    ];


    items1= List<String>.generate(20, (i) => "içerik $i");
    items2= List<String>.generate(10, (i) => "Item $i");
    tumListe=[futureOzet,futureOzet1,futureOzet2];
    //print('deneme');
    //print(tumListe.length);

    /*print('ggg');
    tumListe=[futureOzet,futureOzet1,futureOzet2];
    print(tumListe[parentIndex].length);*/
    double height = 180.0;


    return SizedBox(
      height: height,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          //itemCount: 20,
          itemCount: tumListe[parentIndex].length,


          itemBuilder: (BuildContext content, int index) {
            return _buildItem(
              index: index,
              //itm:items2,
              //itm:futureOzet,
              itm:tumListe[parentIndex],
              //color: colors[(parentIndex + index) % colors.length],
              color: colors[parentIndex],
              parentSize: height,
            );
          }),
    );
  }

  Widget _buildItem({int index, List<Ozet> itm, Color color, double parentSize}) {
    double edgeSize = 5.0;

    //print(index);
    //double itemSize = parentSize - edgeSize * 2.0;
    double itemSize = 200;
    return Container(
      padding: EdgeInsets.all(edgeSize),
      child: SizedBox(
        width: 180,
        height: itemSize,
        child: Container(
            alignment: AlignmentDirectional.center,
            color: color,


            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,


              children: <Widget>[
                Text('${itm[index].baslik}',style: TextStyle(fontSize: 20.0, color: Colors.black)),
                Text('${itm[index].sayi}',style: TextStyle(fontSize: 35.0, color: Colors.white)),



              ],

            )

        ),
      ),
    );
  }

}
