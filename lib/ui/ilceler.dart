class Ilceler {
  final String id;
  final String baslik;
  final String bas;
  final String cay;
  final String dar;
  final String der;
  final String dil;
  final String geb;
  final String gol;
  final String izm;
  final String kan;
  final String karm;
  final String kar;
  final String kor;

  Ilceler({this.id, this.baslik, this.bas, this.cay, this.dar, this.der, this.dil, this.geb, this.gol, this.izm, this.kan, this.karm, this.kar, this.kor});

  factory Ilceler.fromJson(Map<String, dynamic> json) {
    return Ilceler(
      id: json['sno'],
      baslik: json['baslik'],
      bas: json['bas'],
      cay: json['cay'],
      dar: json['dar'],
      der: json['der'],
      dil: json['dil'],
      geb: json['geb'],
      gol: json['gol'],
      izm: json['izm'],
      kan: json['kan'],
      karm: json['karm'],
      kar: json['kar'],
      kor: json['kor'],
    );
  }
}

class Ozet {
  final String id;
  final String baslik;
  final String sayi;
  final String kategori;


  Ozet({this.id, this.baslik, this.sayi, this.kategori});

  factory Ozet.fromJson(Map<String, dynamic> json) {
    return Ozet(
      id: json['sno'],
      baslik: json['baslik'],
      sayi: json['sayi'],
      kategori: json['kategori'],

    );
  }
}