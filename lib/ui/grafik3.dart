import 'package:flutter/material.dart';
//import 'package:flutter/services.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'drawer.dart';
import 'ilceler.dart';

class graph3 extends StatefulWidget {
  static const String route = 'grafik3';

 // charts_flutter: ^0.7.0

  @override
  State<StatefulWidget> createState() => new _NoteScreenState();
}


class _NoteScreenState extends State<graph3> {
  Widget chartContainer = Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [Text('Chart Viewer')],
  );

  List<Ilceler> futureRehber = [];
  List countries = [];
  ValueNotifier<bool> trackingNotifier = ValueNotifier(false);

  // filtre baslik
  void _filterBaslik(value) {
    setState(() {
        futureRehber =countries
            .where((deger) =>
            deger.baslik.toLowerCase().contains(value.toLowerCase()) )
            .toList();
      }
    );
  }


  @override
  void initState() {


    getAllRehber().then((data) {
      setState(() {

        countries = futureRehber = data;

      });
    });

    super.initState();
  }

  Future<List<Ilceler>> getAllRehber() async {
    var res;
    List liste = [];
    final response = await http.get(Uri.parse('http://10.0.2.2/phpmyadmin/istatistik/get_data.php'));
    //print('getEmployees Response: ${response.body}');
    if (response.statusCode == 200) {

      List<Ilceler> list = parseResponse(response.body);
      //print('deneme');
      return list;

    }else {
      return List<Ilceler>();
    }
  }

  static List<Ilceler> parseResponse(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Ilceler>((json) => Ilceler.fromJson(json)).toList();
  }

  @override
  Widget build(BuildContext context) {
    //SystemChrome.setPreferredOrientations([DeviceOrientation.landscapeLeft]);
    return Scaffold(

      // alt buton
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              trackingNotifier.value = !trackingNotifier.value;
            });
          },

          child: ValueListenableBuilder<bool>(
          valueListenable: trackingNotifier,
          builder: (ctx, isTracking, _) {
            if (isTracking) {
              return Icon(Icons.view_array_outlined);
            }
            return Icon(Icons.view_carousel_outlined);
          },
        ),
        ),
        // alt buton


        appBar: AppBar(



          title: TextField(
            onChanged: (value) {         // keliime değiştikçe filtre uygula
              // _filterOkul(value);   //
              _filterBaslik(value);   //
            },
            style: TextStyle(color: Colors.black),
            decoration: InputDecoration(
                icon: Icon(
                  Icons.search,
                  color: Colors.black,
                ),
                hintText: "başlık ara",
                hintStyle: TextStyle(color: Colors.black26)),
          ),

          // ******  ÜST BUTON  ******
        actions: <Widget>[



          trackingNotifier.value

          ? TextButton(
            onPressed: () {
              setState(() {
                trackingNotifier.value = !trackingNotifier.value;
              });
            },
            style: TextButton.styleFrom(
              primary: Colors.white,
            ),
            child: Text(
              'Daralt',
              style: TextStyle(fontSize: 15),
            ),
          )

          : TextButton(
            onPressed: () {
              setState(() {
                trackingNotifier.value = !trackingNotifier.value;
              });
            },
            style: TextButton.styleFrom(
              primary: Colors.white,
            ),
            child: Text(
              'Genişlet',
              style: TextStyle(fontSize: 15),
            ),
          )



         ],
        // ******  ÜST BUTON  ******


        ),

        drawer: buildDrawer(context, graph3.route),
        body: FutureBuilder<List>(

         future: getAllRehber(),
         initialData: List(),
         builder: (context, snapshot) {
         return snapshot.hasData ?

            new ListView.builder(
                 //scrollDirection:  Axis.horizontal,
                 padding: const EdgeInsets.all(10.0),

                 itemCount: futureRehber.length,

                  itemBuilder: ( context, i) {
                      if (trackingNotifier.value==true) {

                         return _buildRow3(futureRehber[i], i);

                      }
                      else
                        return _buildRow2(futureRehber[i], i);

                   },

          )
             : Center(
           child: CircularProgressIndicator(),
         );

         },
        )

    );



}


// 1.YOL **********************************************************
  Widget _buildRow2(Ilceler deger,int index) {
    return new Card(
      //color: index%2==0 ? Colors.white : Colors.orangeAccent,
        color: Colors.white,

        elevation: 4,

        child: ListTile(
          //leading: Icon(Icons.perm_contact_calendar),

          title: Text('${deger.baslik}', style: TextStyle(color: Colors.blueAccent, fontSize: 16.0)),
          //subtitle: Text('${deger.name}'),
          subtitle: Column(
            //mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                    Container(
                    height: 300,
                    //width: 2200,
                    //child: chartContainer,
                    child: chartContainer=VerticalBarLabelChart.withSampleData(deger),

                    ),
              ]),
          //trailing: Icon(Icons.add),
          onTap: () {


          },
        )

    );

  }

// 1.YOL **********************************************************


// 2.YOL **********************************************************
  Widget _buildRow3(Ilceler deger,int index) {
    var colors = [
      Colors.green,
      Colors.blue,
      Colors.indigo,
      Colors.red,
      Colors.orange
    ];

    // Yükseklik
    double height = 400.0;


    return SizedBox(
      height: height,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          //itemCount: 20,
          itemCount: 1,
          // index 1 den dizi boyutu kadar
          //parentIndex 1 den 2 ye kadar
          itemBuilder: (BuildContext content, int index) {
            return _buildItem(deger, index   );
          }),
    );
  }
//***
//***
  Widget _buildItem(Ilceler deger,int index) {
    double edgeSize = 5.0;


    //double itemSize = parentSize - edgeSize * 2.0;
    double itemSize = 200;
    return Container(
      padding: EdgeInsets.all(edgeSize),
      child: SizedBox(
        width: 600,
        height: itemSize,
        child: Container(
            alignment: AlignmentDirectional.center,
            //color: color,
            /*child: Text(
            //'$index',
            '${itm[index]}',
            style: TextStyle(fontSize: 72.0, color: Colors.white),
          ),*/

            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,


              children: <Widget>[
                Text('${deger.baslik}',style: TextStyle(fontSize: 26.0, color: Colors.black45)),
                Container(
                  height: 300,
                  //width: 2200,
                  //child: chartContainer,
                  child: chartContainer=VerticalBarLabelChart.withSampleData(deger),

                ),

              ],

            )

        ),
      ),
    );
  }

// 2.YOL **********************************************************


}



class OrdinalSales {
  final String year;
  final int sales;

  OrdinalSales(this.year, this.sales);
}



class VerticalBarLabelChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  VerticalBarLabelChart(this.seriesList, {this.animate});

  /// Creates a [BarChart] with sample data and no transition.
  factory VerticalBarLabelChart.withSampleData(Ilceler deger) {
    return new VerticalBarLabelChart(
      _createSampleData(deger),
      // Disable animations for image tests.
      animate: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new charts.BarChart(

      seriesList,
      animate: animate,

      barRendererDecorator: new charts.BarLabelDecorator<String>(),
      domainAxis: new charts.OrdinalAxisSpec(),
    );
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<OrdinalSales, String>> _createSampleData(Ilceler deger) {
    final data = [
      new OrdinalSales('Baş', int.parse(deger.bas)),
      new OrdinalSales('Çay', int.parse(deger.cay)),
      new OrdinalSales('Dar', int.parse(deger.dar)),
      new OrdinalSales('Der', int.parse(deger.der)),
      new OrdinalSales('Dil', int.parse(deger.dil)),
      new OrdinalSales('Geb', int.parse(deger.geb)),
      new OrdinalSales('Göl', int.parse(deger.gol)),
      new OrdinalSales('İzm', int.parse(deger.izm)),
      new OrdinalSales('Kan', int.parse(deger.kan)),
      new OrdinalSales('Karm', int.parse(deger.karm)),
      new OrdinalSales('Kar', int.parse(deger.kar)),
      new OrdinalSales('Kör', int.parse(deger.kor)),

    ];

    return [
      new charts.Series<OrdinalSales, String>(
          id: 'Sales',
          domainFn: (OrdinalSales sales, _) => sales.year,
          measureFn: (OrdinalSales sales, _) => sales.sales,
          data: data,
          // Set a label accessor to control the text of the bar label.
          labelAccessorFn: (OrdinalSales sales, _) =>
           '${sales.sales.toString()}')
    ];
  }
}