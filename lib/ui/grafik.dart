import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'drawer.dart';


class graph1 extends StatefulWidget {
  static const String route = 'grafik1';


  @override
  State<StatefulWidget> createState() => new _NoteScreenState();
}



class _NoteScreenState extends State<graph1> {

  @override
  Widget build(BuildContext context) {
    //SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return Scaffold(
        appBar: AppBar(title: Text('Grafik1')),
        drawer: buildDrawer(context, graph1.route),
        body: Center(
            child: Container(

                padding: EdgeInsets.only(top: 30),


                child: SfCartesianChart(
                    primaryXAxis: CategoryAxis(),
                    // Chart title
                    title: ChartTitle(text: 'Başlık'),
                    // Enable legend
                    legend: Legend(isVisible: true),
                    // Enable tooltip
                    tooltipBehavior: TooltipBehavior(enable: true),

                    series: <LineSeries<SalesData, String>>[
                      LineSeries<SalesData, String>(
                          dataSource:  <SalesData>[
                            SalesData('Jan', 35),
                            SalesData('Feb', 12),
                            SalesData('Mar', 34),
                            SalesData('Apr', 32),
                            SalesData('May', 60)
                          ],
                          xValueMapper: (SalesData sales, _) => sales.year,
                          yValueMapper: (SalesData sales, _) => sales.sales,
                          // Enable data label
                          dataLabelSettings: DataLabelSettings(isVisible: true)
                      )
                    ]
                )
            )
        )
    );
  }






}

class SalesData {
  SalesData(this.year, this.sales);

  final String year;
  final double sales;
}



