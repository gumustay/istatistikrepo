import 'package:flutter/material.dart';

import 'sliver.dart';
import 'sliver_grid.dart';
import 'ana_sayfa.dart';
import 'grafik.dart';
import 'grafik3.dart';


Widget _buildMenuItem(

    BuildContext context, Widget title, String routeName, String currentRoute) {
  var isSelected = routeName == currentRoute;

  return ListTile(
    title: title,
    selected: isSelected,
    onTap: () {
      if (isSelected) {
        Navigator.pop(context);
      } else {
        Navigator.pushReplacementNamed(context, routeName);
      }
    },
  );
}

Drawer buildDrawer(BuildContext context, String currentRoute) {
  return Drawer(
    child: ListView(
      children: <Widget>[
        const DrawerHeader(
          child: Center(
            child: Text('Kocaeli Milli Eğitim Müdürlüğü İstatistik Verileri',textAlign: TextAlign.center),
          ),
        ),
        _buildMenuItem(
          context,
          const Text('Anasayfa'),
          AnaSayfa.route,
          currentRoute,
        ),
        _buildMenuItem(
          context,
          const Text('Grid'),
          slivergrid.route,
          currentRoute,
        ),
        _buildMenuItem(
          context,
          const Text('Sliver'),
          sliverliste.route,
          currentRoute,
        ),
        _buildMenuItem(
          context,
          const Text('Grafik1'),
          graph1.route,
          currentRoute,
        ),
        _buildMenuItem(
          context,
          const Text('Grafik3'),
          graph3.route,
          currentRoute,
        ),


      ],
    ),
  );
}
